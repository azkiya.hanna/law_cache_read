from django.urls import path
from . import views

app_name = 'main'

urlpatterns = [
    path('', views.index, name='index'),
    path('<int:npm>/', views.read_to_db, name='read_to_db'),
]