from urllib import response
from django.http import JsonResponse
from rest_framework import status
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_exempt
from .models import *
from django.core.exceptions import ObjectDoesNotExist


def index(request):
    return JsonResponse({'message':'this is law cache task'})

@api_view(['GET'])
@csrf_exempt
def read_to_db(request,npm):
    try:
        user = UserModel.objects.get(npm=npm)
        return JsonResponse({'status':'OK','npm': user.npm, 'nama': user.name},status = status.HTTP_200_OK)
    except ObjectDoesNotExist:
        return JsonResponse({'error': 'User does not exist'},status=status.HTTP_404_NOT_FOUND)
